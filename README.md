# kong-openshift
Kong API Gateway for Openshift - v0.10.x

## Create Project

##### Configuration
```
oc create configmap kong-config --from-file=nginx-conf/
```

##### Secret

Update kong-database-config.secret.yaml with the Base64 values. Example :
```
echo -n "db_name" | base64
echo -n "db_password" | base64
echo -n "db_user" | base64

kong-database-name: ZGJfbmFtZQ==
kong-database-password: ZGJfcGFzc3dvcmQ=
kong-database-user: ZGJfdXNlcg==
```
Create Secret
```
oc create -f kong-database-config.secret.yaml
```


##### Kong Database
```
oc create -f kong-database.dc.yaml
oc create -f kong-database.svc.yaml

```

##### Kong Gateway
```
oc create -f kong-gateway.dc.yaml
oc create -f kong-proxy.svc.yaml
oc create -f kong-admin.svc.yaml

```

## Reference
[Kong API Gateway Documentation](https://getkong.org/docs/0.10.x/)
